//
//  MapViewController.swift
//  touristapp
//
//  Created by Gracjan Grabowski on 24/10/2016.
//  Copyright © 2016 Gracjan Grabowski. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController {
    
    @IBOutlet var map: MKMapView!
    
    let locationManager = CLLocationManager()
    let mapToDetailsSegue = "mapToDetails"
    let reuseIdentifier = "pin"
    let currentSpan = 0.2
    let minimumDistance = 500.0
    
    var currentLocation = CLLocation()
    var currentAttraction: TouristAttraction!
    var attractionsMap = [TouristAttraction]()

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        map.delegate = self
        
        loadAttractionToMap()
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        self.map.showsUserLocation = true
    }
    // MARK: - segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if  segue.identifier == mapToDetailsSegue,
            let destinationDetails = segue.destination as? DetailsViewController{
            
            destinationDetails.currentAttraction = currentAttraction
        }
    }

    @IBAction func buttonOpenActionSheetClicked(sender: AnyObject){
        
        var googleMaps = UIAlertAction()
        
        let actionSheet = UIAlertController(title:  NSLocalizedString("Navigation options:", comment: "naviation options title"), message: NSLocalizedString("Choose application to navigate", comment: "navigate option comment"), preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let appleMaps = UIAlertAction(title: NSLocalizedString("Apple maps", comment: "apple maps title"), style: UIAlertActionStyle.default) {
            (ACTION) in
            
            if let url = NSURL(string: "http://maps.apple.com/?saddr=\(self.currentLocation.coordinate.latitude),\(self.currentLocation.coordinate.longitude)&daddr=\(self.currentAttraction.latitude),\(self.currentAttraction.longitude)"){
            
                UIApplication.shared.openURL(url as URL)
            }
        }
        
        if (UIApplication.shared.canOpenURL(NSURL(string:"comgooglemaps://")! as URL)) {
            
            googleMaps = UIAlertAction(title: NSLocalizedString("Google maps", comment: "google maps title"), style: UIAlertActionStyle.default) {
                (ACTION) in

                UIApplication.shared.openURL(NSURL(string: "comgooglemaps://?saddr=\(self.currentLocation.coordinate.latitude),\(self.currentLocation.coordinate.longitude)&daddr=\(self.currentAttraction.latitude),\(self.currentAttraction.longitude)&directionsmode=driving") as! URL)
            }
            
         actionSheet.addAction(googleMaps)
        }
        
        let cancelButton = UIAlertAction(title: NSLocalizedString("Cancel", comment: "cancel button action sheet"), style: UIAlertActionStyle.cancel) {
            (ACTION) in
        }

        actionSheet.addAction(appleMaps)
        actionSheet.addAction(cancelButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
}

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation is MKUserLocation {
            return nil
        }
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: reuseIdentifier) as? MKPinAnnotationView
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: reuseIdentifier)
            annotationView?.canShowCallout = true
            annotationView?.rightCalloutAccessoryView = UIButton(type: UIButtonType.detailDisclosure)
            
            let navButtonImage = UIImage(named: "naviButtonx2") as UIImage?
            let button = UIButton(type: UIButtonType.custom) as UIButton
            button.frame = CGRect(x: 100, y: 100, width: 100, height: 30)
            button.setImage(navButtonImage, for: .normal)
            button.addTarget(self, action: #selector(buttonOpenActionSheetClicked), for: .touchUpInside)
            
            annotationView?.leftCalloutAccessoryView = button
        }
        
        if annotation is AttractionAnnotation {
            annotationView?.pinTintColor = (annotation as! AttractionAnnotation).attraction.colorPin()
        }
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl){
        
        var selectedAttractionAnnotation: AttractionAnnotation
        
        if control == view.rightCalloutAccessoryView {
            selectedAttractionAnnotation = view.annotation as! AttractionAnnotation
            currentAttraction = selectedAttractionAnnotation.attraction
            performSegue(withIdentifier: mapToDetailsSegue, sender: self)
        }
        if control == view.leftCalloutAccessoryView {
            selectedAttractionAnnotation = view.annotation as! AttractionAnnotation
            currentAttraction = selectedAttractionAnnotation.attraction
        }
    }
    
    func loadAttractionToMap(){
        
        for atr in attractionsMap {
            let location = CLLocationCoordinate2DMake(atr.latitude, atr.longitude)
            let span = MKCoordinateSpanMake(currentSpan, currentSpan)
            let region = MKCoordinateRegion(center: location, span: span)
            let annotation = AttractionAnnotation(attraction: atr, title: atr.name, subtitle: nil, coordinate: location)
            
            map.setRegion(region, animated: true)
            map.addAnnotation(annotation)
        }
    }
}

extension MapViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.last!
        let center = CLLocationCoordinate2DMake(currentLocation.coordinate.latitude, currentLocation.coordinate.longitude)
        let region = MKCoordinateRegion(center: center, span: MKCoordinateSpanMake(currentSpan, currentSpan))
        self.map.setRegion(region, animated: true)
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error.localizedDescription)")
    }
    
    func caluclateDistance(atr: TouristAttraction) -> Bool {
        
        let userCoordinate = CLLocation(latitude: currentLocation.coordinate.latitude ,longitude: currentLocation.coordinate.longitude)
        let attractionCoordinate = CLLocation(latitude: atr.latitude, longitude: atr.longitude)
        let distance = userCoordinate.distance(from: attractionCoordinate)
        
        if (distance <= minimumDistance){
            return true;
        }
        
        return false;
    }
}
