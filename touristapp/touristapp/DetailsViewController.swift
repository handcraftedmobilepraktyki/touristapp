//
//  DetailsViewController.swift
//  touristapp
//
//  Created by Gracjan Grabowski on 14/10/2016.
//  Copyright © 2016 Gracjan Grabowski. All rights reserved.
//

import UIKit
import Social
import MessageUI
import MapKit

class DetailsViewController: UIViewController, UINavigationControllerDelegate{

    //MARK: Properties

    @IBOutlet weak var imageAttraction: UIImageView!
    @IBOutlet weak var pickedImage: UIImageView!
    @IBOutlet weak var ViewWitMap: UIView!
    @IBOutlet weak var ViewWithLabel: UIView!
    @IBOutlet weak var ViewWithImage: UIView!
    @IBOutlet weak var ViewWithTakePhoto: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var longDsc: UILabel!
    @IBOutlet weak var backToList: UIButton!
    @IBOutlet weak var photoButton: UIButton!
    @IBOutlet weak var mapButton: UIButton!
    @IBOutlet weak var descButton: UIButton!
    @IBOutlet var mapView: MKMapView!

    var visibleTakePhotoButton = false
    var currName = String()
    var currentAttraction: TouristAttraction!
    
    let jpegHighQuality = 1.0
    let currentSpan = 0.05
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
        super.viewWillAppear(animated)
        
        ViewWitMap.isHidden = true
        ViewWithImage.isHidden = true
        ViewWithLabel.isHidden = true
        ViewWithTakePhoto.isHidden = true
        
        let fileURL = NSURL(string: (currentAttraction?.photoURL)!)

        nameLabel.text = (currentAttraction?.name)!
        longDsc.text = (currentAttraction?.longDescription)!
        imageAttraction.sd_setImage(with: fileURL as URL!)
        currName = "\((currentAttraction?.name)!).jpeg"
        
        loadImageFromAppFolder(filename: currName)
        
        let location = CLLocationCoordinate2D(latitude: currentAttraction.latitude, longitude: currentAttraction.longitude)
        let span = MKCoordinateSpan(latitudeDelta: currentSpan, longitudeDelta: currentSpan)
        let region = MKCoordinateRegion(center: location, span: span)
        let annotation = AttractionAnnotation(attraction: currentAttraction, title: currentAttraction.name, subtitle: nil, coordinate: location)
        
        mapView.setRegion(region, animated: true)
        mapView.addAnnotation(annotation)
        self.mapView.showsUserLocation = true
    }

    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
        super.viewWillAppear(animated)
    }
    
    //MARK: Buttons methods
    
    @IBAction func backToListTap(_ sender: Any) {
        
        navigationController?.popViewController(animated: true)
    }

    @IBAction func shareButtonAction(_ sender: Any) {
        
        let actionSheet = UIAlertController(title: "", message: NSLocalizedString("Share via:", comment: "share comment"), preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let facebook = UIAlertAction(title: "Facebook", style: UIAlertActionStyle.default) {
            (ACTION) in
            
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeFacebook){
                let facebookComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
                
                if self.pickedImage.image != nil {
                    
                    facebookComposeViewController?.add(self.pickedImage.image)
                }
                
                self.present(facebookComposeViewController!, animated: true, completion: nil)
            }
            else{
                self.showAlertMessage(message: NSLocalizedString("You are not logged in to your Facebook account", comment: "Facebook error login info"))
            }
        }
        
        let twitter = UIAlertAction(title: "Twitter", style: UIAlertActionStyle.default) {
            (ACTION) in
            
            if SLComposeViewController.isAvailable(forServiceType: SLServiceTypeTwitter){
                let twitterComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
                
                twitterComposeViewController?.setInitialText(NSLocalizedString("Hi! I'm here ", comment:"say hello info") + (self.currentAttraction.name) + (self.currentAttraction.longDescription))
                
                self.present(twitterComposeViewController!, animated: true, completion: nil)
            }
            else{
                self.showAlertMessage(message: NSLocalizedString("You are not logged in to your Twitter account", comment: "Twitter error login info"))
            }
        }

        let mail = UIAlertAction(title:"Mail", style: UIAlertActionStyle.default) {
            (ACTION) in
            
            if !MFMailComposeViewController.canSendMail(){
                self.showAlertMessage(message: "Mail")
            }
                
                let mailComposeViewController = MFMailComposeViewController()
                    mailComposeViewController.mailComposeDelegate = self
                
                mailComposeViewController.setMessageBody(NSLocalizedString("Hi! I'm here", comment: "say hello info")  +  (self.currentAttraction.name), isHTML: false)
                
                if self.pickedImage.image != nil {
                    
                    let imageToSend = UIImageJPEGRepresentation(self.pickedImage.image!, CGFloat(self.jpegHighQuality))
                    
                   mailComposeViewController.addAttachmentData(imageToSend!, mimeType: "application/jpeg", fileName: "\(self.currentAttraction.name).jpeg")
                }
                
            self.present(mailComposeViewController, animated: true, completion: nil)
        }
        
 
        let cancelButton = UIAlertAction(title: NSLocalizedString("Cancel", comment: "cancel button action sheet"), style: UIAlertActionStyle.cancel) {
            (ACTION) in
        }
        
        actionSheet.addAction(facebook)
        actionSheet.addAction(twitter)
        actionSheet.addAction(mail)
        actionSheet.addAction(cancelButton)
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func showAlertMessage(message: String!) {
        let alertController = UIAlertController(title: NSLocalizedString("Sharing error", comment: "sharing error alert"), message: message, preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "cancel alert info"), style: UIAlertActionStyle.default, handler: nil))
        self.present(alertController, animated: true, completion: nil)
    }
    
    
    @IBAction func descButtonTap(_ sender: Any) {
        
        ViewWitMap.isHidden = true
        ViewWithImage.isHidden = true
        ViewWithLabel.isHidden = false
        longDsc.sizeToFit()
    }
 
    
    @IBAction func mapButtonTap(_ sender: Any) {
        ViewWitMap.isHidden = false
        ViewWithImage.isHidden = true
        ViewWithLabel.isHidden = true
    }
    
    @IBAction func photoButtonTap(_ sender: Any) {

        ViewWitMap.isHidden = true
        ViewWithImage.isHidden = false
        ViewWithLabel.isHidden = true
        
        if pickedImage.image == nil {
            ViewWithImage.isHidden = true
            ViewWithTakePhoto.isHidden = false
        }
    }
    
    //MARK: save/load photo methods
    func savePhotoToPhotoLibrary(){
        
        let imageData = UIImageJPEGRepresentation(pickedImage.image!, CGFloat(jpegHighQuality))
        let compressedImage = UIImage(data: imageData!)
        
        UIImageWriteToSavedPhotosAlbum(compressedImage!, nil, nil, nil)
    }
    
    func savePhotoToAppFolder(filename: String){
        let path = try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
        let newPath = path.appendingPathComponent(filename)
        let imageData = UIImageJPEGRepresentation(pickedImage.image!, CGFloat(jpegHighQuality))
        
        do {
            try imageData!.write(to: newPath, options: .atomic)
        }
        catch {
            let alert = UIAlertController(title: NSLocalizedString("Error", comment: "error save photo"), message: NSLocalizedString("Error while saving photo.", comment: "error save data description"), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func loadImageFromAppFolder(filename: String){

        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let fileUrl = documentsDirectory.appendingPathComponent(filename)
        let urlString: String = fileUrl.path
        let image = UIImage(contentsOfFile: urlString)
        
        if image != nil {
            pickedImage.image = image
        }
    }
}

extension DetailsViewController: MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController,
                               didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result.rawValue {
        case MFMailComposeResult.sent.rawValue:
            showMailMessage(message: NSLocalizedString("Sent", comment: "sent mail info"), controller: controller)
        case MFMailComposeResult.cancelled.rawValue:
            showMailMessage(message: NSLocalizedString("Cancelled", comment: "cancel mail info"), controller: controller)
        case MFMailComposeResult.saved.rawValue:
            showMailMessage(message: NSLocalizedString("Saved", comment: "saved mail info"), controller: controller)
        case MFMailComposeResult.failed.rawValue:
            showMailMessage(message: NSLocalizedString("Error", comment: "Error mail info") + (error?.localizedDescription)!, controller: controller)
            controller.dismiss(animated: true, completion:  nil)
        default:
            break
        }
    }
    
    func showMailMessage(message: String!, controller: MFMailComposeViewController){
        
        let alertController = UIAlertController(title: NSLocalizedString("Mail status:", comment: "Mail status alert"), message: message, preferredStyle: .alert)
        let okButton = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alertController.addAction(okButton)
        controller.dismiss(animated: true){ () -> Void in
        self.present(alertController, animated: true, completion: nil)
        }
    }
}

extension DetailsViewController: UIImagePickerControllerDelegate {
    
    @IBAction func takePhoto(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.camera){
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = false
            
            self.present(imagePicker, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingImage image: UIImage!, editingInfo: [NSObject : AnyObject]) {
        
        pickedImage.image = image
        
        savePhotoToPhotoLibrary()
        savePhotoToAppFolder(filename: currName)
        
        self.dismiss(animated: true, completion: nil)
    }
}
