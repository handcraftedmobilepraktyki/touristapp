//
//  AttractionTableViewController.swift
//  touristapp
//
//  Created by Gracjan Grabowski on 10/10/2016.
//  Copyright © 2016 Gracjan Grabowski. All rights reserved.
//

import UIKit
import SDWebImage
import Alamofire
import CoreLocation
import ContentfulDeliveryAPI

class AttractionTableViewController: UITableViewController {

    
    let detailsSeque = "detailsSegue"
    let mapSegue = "showMap"
    let locationManager = CLLocationManager()
    let client = CDAClient(spaceKey: "4o6al86apcwg", accessToken: "293cbb4fce22f7a74f71cce1aa4bdaa544d0832e29ecc19d748c4a19945997b8")
    
    var attractions = [TouristAttraction]()
    var currentRow = 0
    var searchURL = "http://touristapp.cba.pl/pliki/current2.txt"
    var currentLocation = CLLocation()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyBest
        self.locationManager.requestWhenInUseAuthorization()
        self.locationManager.startUpdatingLocation()
        
        
        client.fetchEntries(matching: ["content_type": "attraction"], success: { response, entry in

            for item in entry.items {
                
                let attractionType = (item as AnyObject).fields["attractionType"] as! String
                let name = (item as AnyObject).fields["name"] as! String
                let photoUrl = (item as AnyObject).fields["photoUrl"] as! String
                let shortDescription = (item as AnyObject).fields["shortDescription"] as! String
                let longDescription = (item as AnyObject).fields["longDescription"] as! String
                let coordinateData = (item as AnyObject).fields["coordinate"] as! NSData
                var coordinate = CLLocationCoordinate2D(latitude: 0, longitude: 0)
                coordinateData.getBytes(&coordinate, length: MemoryLayout<CLLocationCoordinate2D>.size)
                
                
                let attraction = TouristAttraction(attractionType: attractionType, name: name, shortDescription: shortDescription, longDescription: longDescription, photoURL: photoUrl, latitude: coordinate.latitude, longitude: coordinate.longitude)!
                
                self.attractions.append(attraction)
                self.tableView.reloadData()
            }
        },
           failure: {response, error in
            let alert = UIAlertController(title: NSLocalizedString("Error", comment: "error load data"), message: NSLocalizedString("Error while downloading data.\n Please check your Internet connection.", comment: "error load data description"), preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            
        })
   }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return attractions.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) ->UITableViewCell {
        
        let cellIdentifier = "AttractionTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath) as! AttractionTableViewCell
        let attractionRow = attractions[indexPath.row]
        let fileUrl = NSURL(string: attractionRow.photoURL)
        
        cell.nameLabel.text = attractionRow.name
        cell.shortDescription.text = attractionRow.shortDescription
        cell.miniaturePhoto.sd_setImage(with: fileUrl as URL!)
        cell.textFieldDistance.text = "\(caluclateDistance(atr: attractionRow))"
        cell.iconView.image = attractionRow.setIcon()
        
        return cell
    }
    
    @IBAction func attractionsMapSelected(_ sender: AnyObject) {
        
        performSegue(withIdentifier: mapSegue, sender: self)
        
    }
    //MARK: Navigations
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if  segue.identifier == detailsSeque,
            let destinationDetails = segue.destination as? DetailsViewController{
            
            destinationDetails.currentAttraction = attractions[currentRow]
        }
        
        if segue.identifier == mapSegue,
          let destinationMap = segue.destination as? MapViewController {
            
                destinationMap.attractionsMap = attractions
        }
        
    }

    // MARK: - UITableViewDelegate Methods

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        currentRow = indexPath.row
        performSegue(withIdentifier: detailsSeque, sender: self)
    }
}
extension AttractionTableViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        currentLocation = locations.last!
        self.locationManager.stopUpdatingLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Error: \(error.localizedDescription)")
    }

    func caluclateDistance(atr: TouristAttraction) -> String{
        
        let userCoordinate = CLLocation(latitude: currentLocation.coordinate.latitude ,longitude: currentLocation.coordinate.longitude)
        let attractionCoordinate = CLLocation(latitude: atr.latitude, longitude: atr.longitude)
        let distance = userCoordinate.distance(from: attractionCoordinate)
        var distanceInfo = ""
        
        if(Int(distance)/1000 == 0 ) {
            distanceInfo = "\(Int(distance)) m"
        }
        else {
            distanceInfo = "\(Int(distance)/1000) km"
        }
        
    return distanceInfo
    }
}
