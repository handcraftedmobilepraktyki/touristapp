//
//  AttractionAnnotation.swift
//  touristapp
//
//  Created by Gracjan Grabowski on 25/10/2016.
//  Copyright © 2016 Gracjan Grabowski. All rights reserved.
//

import UIKit
import MapKit

class AttractionAnnotation: NSObject, MKAnnotation {
    
    var attraction: TouristAttraction
    var title: String?
    var subtitle: String?
    var coordinate: CLLocationCoordinate2D
    
    init(attraction: TouristAttraction, title: String?, subtitle: String?, coordinate: CLLocationCoordinate2D)
    {
        self.attraction = attraction
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        
        super.init()
    }
}
