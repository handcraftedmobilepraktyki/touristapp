//
//  AttractionTableViewCell.swift
//  touristapp
//
//  Created by Gracjan Grabowski on 10/10/2016.
//  Copyright © 2016 Gracjan Grabowski. All rights reserved.
//

import UIKit

class AttractionTableViewCell: UITableViewCell {

    
    //MARK: Properties
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var shortDescription: UILabel!
    @IBOutlet weak var miniaturePhoto: UIImageView!
    @IBOutlet weak var textFieldDistance: UITextField!
    @IBOutlet weak var iconView: UIImageView!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        miniaturePhoto.layer.cornerRadius = miniaturePhoto.frame.height / 2
        miniaturePhoto.clipsToBounds = true
    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
