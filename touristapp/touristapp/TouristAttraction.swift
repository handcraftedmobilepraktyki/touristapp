//
//  TouristAttraction.swift
//  touristapp
//
//  Created by Gracjan Grabowski on 10/10/2016.
//  Copyright © 2016 Gracjan Grabowski. All rights reserved.
//

import UIKit

class TouristAttraction {
    var attractionType: String
    var name: String
    var shortDescription: String
    var longDescription: String
    var photoURL: String
    var latitude: Double
    var longitude: Double
    
    
    init?(attractionType: String, name: String, shortDescription: String, longDescription: String, photoURL: String, latitude: Double, longitude: Double){
        self.attractionType = attractionType
        self.name = name
        self.shortDescription = shortDescription
        self.longDescription = longDescription
        self.photoURL = photoURL
        self.latitude = latitude
        self.longitude = longitude
        
        
        if name.isEmpty{
            return nil
        }
    }
    func colorPin() -> UIColor {
        switch attractionType {
            
        case "entertaiment":
            return .red
        case "museum":
            return .green
        case "food":
            return .purple
        default:
            return .yellow
        }
    }
    func setIcon() -> UIImage{
        switch attractionType{
            case "entertaiment":
            return #imageLiteral(resourceName: "entertaimentIcon")
        case "museum":
            return #imageLiteral(resourceName: "museumIcon")
        case "food":
            return #imageLiteral(resourceName: "foodIcon")
        default:
            return #imageLiteral(resourceName: "noIcon")
        }
    }
}
